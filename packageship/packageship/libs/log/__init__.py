#!/usr/bin/python3
"""
Common methods for logging
"""
from packageship.libs.log.loghelper import setup_log
from packageship.libs.log.loghelper import Log, LOGGER

__all__ = ['setup_log', 'Log', 'LOGGER']
